﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SHSWebServer.DTO;
using SHSWebServer.Models;
using System;

namespace SHSWebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LotInfoController : ControllerBase
    {
        [HttpPost]
        public ActionResult<Message> Post(LotInfoDto lotInfo)
        {
            Message message = new Message();
            message.Code = (int)MessageState.Success;
            message.Msg = "成功";
            if (!Global.SHSClien.IsConnect)
            {
                message.Code = (int)MessageState.Fail;
                message.Msg = "数据终端软件未启动，请检查";
            }
            else
            {
                try
                {
                    Global.SHSClien.SetLotInfo(lotInfo);
                }
                catch (Exception ex)
                {
                    message.Code = (int)MessageState.Fail;
                    message.Msg = $"向数据终端转发数据失败，{ex.Message},请检查";
                }
            }

            return Ok(message);
        }
    }
}
