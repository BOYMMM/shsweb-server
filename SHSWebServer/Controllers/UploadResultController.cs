﻿using Microsoft.AspNetCore.Mvc;
using SHSWebServer.DAO;
using SHSWebServer.DTO;
using SHSWebServer.Models;

namespace SHSWebServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UploadResultController:ControllerBase
    {
        [HttpPost]
        public ActionResult<Message> Post(AlloyResultDto result)
        {
            Message message = new Message();
            message.Code = (int)MessageState.Success;
            message.Msg = "成功";

            return Ok(message);
        }
    }
}
