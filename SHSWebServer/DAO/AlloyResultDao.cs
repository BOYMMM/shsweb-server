﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeSql.DataAnnotations;

namespace SHSWebServer.DAO
{
    [Table(Name = "AlloyResult")]
    public class AlloyResultDao
    {
        [Column(IsIdentity = true, IsPrimary = true)]
        public int ID {get;set;}
        public string LotID { get; set; }

        public string ClientID{ get; set; }
        public string ResultState{ get; set; }
        public DateTime DateTime{ get; set; }
        [Column(StringLength =-1)]
        public string ElementResults { get; set; }
        public bool IsUpload{ get; set; }
        public DateTime UploadDateTime{ get; set; }
    }
}
