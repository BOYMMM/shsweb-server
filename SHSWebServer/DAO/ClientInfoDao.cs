﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeSql.DataAnnotations;

namespace SHSWebServer.DAO
{
    [Table(Name = "ClientInfo")]
    public class ClientInfoDao
    {
        [Column(IsPrimary = true)]
        public string ClientId { get; set; }
        public string IP { get; set; }
        public DateTime RegisterTime { get; set; }

        public string MAC { get; set; }

        /// <summary>
       /// 轧制号
       /// </summary>
        public string LotId { get; set; }
    }
}
