﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeSql.DataAnnotations;

namespace SHSWebServer.DAO
{
    [Table(Name = "LotInfo")]
    public class LotInfoDao
    {
        [Column(IsIdentity = true, IsPrimary = true)]
        
        public int ID { get; set; }
        
        /// <summary>
        /// 任务接收时间
        /// </summary>
        public DateTime TaskReceiveDateTime { get; set; }
        /// <summary>
        /// 任务下发时间
        /// </summary>
        public DateTime TaskSendDateTime { get; set; }

       /// <summary>
       /// 轧制号
       /// </summary>
        public string LotId { get; set; }
        /// <summary>
        /// 炉号
        /// </summary>
        public string FurnaceNumber { get; set; }
        /// <summary>
        /// 钢种
        /// </summary>
        public string AlloyName { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        public string Norm { get; set; }
        /// <summary>
        /// 计划数
        /// </summary>
        public int PlannedCount { get; set; }
        /// <summary>
        /// 元素信息
        /// </summary>
        [Column(StringLength =-1)]
        public string ElementInfos { get; set; }
    }
}
