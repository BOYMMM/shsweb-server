﻿using FreeSql.DataAnnotations;

namespace SHSWebServer.DAO
{
    public class SystemConfig
    {
        [Column(IsIdentity = true, IsPrimary = true)]
        public int ID { get; set; }
        public string SSID { get; set; } = "";
        public string Password { get; set; } = "";

        public string ServerIp { get; set; } = "";
        public int ServerPort { get; set; } = 5000;

        public string ClientID { get; set; } = "";
    }
}
