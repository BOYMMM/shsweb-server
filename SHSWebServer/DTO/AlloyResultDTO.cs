﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHSWebServer.DTO;

namespace SHSWebServer.DTO
{
    public class AlloyResultDto
    {
        public string LotID { get; set; }

        public string ClientID{ get; set; }
        public DateTime DateTime{ get; set; }
        public string ResultState{ get; set; }
        public ElementResult[] ElementResults { get; set; }
    }
}
