﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHSWebServer.DTO
{
    public class ClientInfoDto
    {
        public string ClientId { get; set; }
        public string IP { get; set; }

        public string MAC { get; set; }
    }
}
