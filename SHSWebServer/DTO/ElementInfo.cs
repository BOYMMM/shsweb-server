﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHSWebServer.DTO
{
    public class ElementInfo
    {
        public string Name { get; set; }
        public double Max { get; set; }
        public double Min { get; set; }
    }
}
