﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHSWebServer.DTO
{
    public class ElementResult
    {
        public string Name { get; set; }
        public double Value { get; set; }
        public double Error { get; set; }

        public string ResultState{ get; set; }
    }
}
