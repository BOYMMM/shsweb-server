﻿namespace SHSWebServer.DTO
{
    public class HeartbeatPackage
    {
        public string DateTime { get; set; }
    }
}
