﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHSWebServer.DTO
{
    public class LotInfoDto
    {
        public string LotID { get; set; }
        public string Alloy { get; set; }
        public int PlannedCount  { get; set; }
        public int PassCount  { get; set; }
        public int FailCount  { get; set; }
        public string FurnaceNumber{ get; set; }
        public string Norm  { get; set; }
        public ElementInfo[] ElementInfos { get; set; }

        public string Line { get; set; }
        public string Standard { get; set; }
    }
}
