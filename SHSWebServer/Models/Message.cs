﻿namespace SHSWebServer.Models
{
    public class Message
    {
        /// <summary>
        /// 0--失败  1--成功
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// 失败信息
        /// </summary>
        public string? Msg { get; set; }

        public object? Data { get; set;}
    }

    public enum MessageState
    {
        Fail,
        Success
    }
}
