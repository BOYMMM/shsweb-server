
using MySqlX.XDevAPI.Common;
using Newtonsoft.Json;
using RestSharp;
using SHSWebServer.DAO;
using SHSWebServer.DTO;
using SHSWebServer.Models;
using SocketTool;
using SuperSimpleTcp;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace SHSWebServer
{
    public class Program
    {
        static Task uploadResultTask;
        static bool loopFlag = true;
        static RestClient restClient = null;
        public static void Main(string[] args)
        {
            Global.Logger = NLog.LogManager.GetCurrentClassLogger();
            Global.Logger.Info($"软件启动,版本:{Assembly.GetEntryAssembly().GetName().Version}");

            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.WebHost.ConfigureKestrel(serverOptions =>
            {
                // 设置Kestrel监听的IP地址和端口
                serverOptions.ListenAnyIP(5000); // 监听任意IP的5000端口
                // 或者指定具体的IP地址
                // serverOptions.Listen(IPAddress.Parse("192.168.1.2"), 5000);
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();
            var client = new SocketTool.TcpClient("127.0.0.1",10934);
            client.Connect();
            client.ReceivedMessageEvent += ReceivedMessage;   
            client.TcpStateChangedEvent +=  TcpStateChanged;
            Global.SHSClien = new SHSClient(client);
           
            Global.ResultDb = new FreeSql.FreeSqlBuilder()
            .UseConnectionString(FreeSql.DataType.MySql, $"Data Source=127.0.0.1;Port=3306;User ID=root;Password=mb115811; Initial Catalog=SHS;Charset=utf8;SslMode=None;")
            .UseAutoSyncStructure(true) //自动同步实体结构到数据库，FreeSql不会扫描程序集，只有CRUD时才会生成表。
            .Build();
            uploadResultTask = new Task(UploadResult);
            uploadResultTask.Start();
            app.Run();
        }

        private static void UploadResult()
        {
            SystemConfig systemConfig = new SystemConfig();
            while (loopFlag)
            {
                try
                {
                    //获取新的系统设置
                    var systemConfigs = Global.ResultDb.Select<SystemConfig>().Where(p=>p.ID==1).ToList();
                    if (systemConfigs.Count == 0 || string.IsNullOrEmpty(systemConfigs[0].ServerIp))
                    {
                        Thread.Sleep(1000);
                        continue;
                    };
                    if(systemConfig.ServerIp != systemConfigs[0].ServerIp ||
                        systemConfig.ServerPort != systemConfigs[0].ServerPort)
                    {
                        restClient?.Dispose();
                        restClient = new RestClient($"http://{systemConfigs[0].ServerIp}:{systemConfigs[0].ServerPort}");
                            
                        Global.Logger.Info($"Server:{systemConfigs[0].ServerIp}:{systemConfigs[0].ServerPort}");
                    }
                    systemConfig = systemConfigs[0];


                    //1.查询是否有未上传记录
                    if (!Global.ResultDb.Select<AlloyResultDao>().Any(p=>p.IsUpload!=true))
                    {
                        Thread.Sleep(1000);
                        continue;
                    }
                    var results = Global.ResultDb.Select<AlloyResultDao>().Where(p=>p.IsUpload!=true).Page(1, 20).ToList();        
                    Global.Logger.Info($"未上传数据:{results.Count}条");
                    foreach (var result in results)
                    {
                        AlloyResultDto alloyResultDto = new AlloyResultDto
                        {
                            ClientID = result.ClientID,
                            LotID = result.LotID,
                            DateTime = result.DateTime,
                            ResultState = result.ResultState
                        };
                        if (!string.IsNullOrEmpty(result.ElementResults))
                        {
                            alloyResultDto.ElementResults = JsonConvert.DeserializeObject<ElementResult[]>(result.ElementResults);
                        }
                        try
                        {
                            //2.上传
                            var request = new RestRequest("UploadResult");
                            request.AddJsonBody(alloyResultDto);
                            //Global.Logger.Info($"UploadResult:{result.LotID}-{result.DateTime:yyyyMMdd HHmmss}");
                            string str = JsonConvert.SerializeObject(alloyResultDto);
                            var response = restClient.Post<Message>(request);
                            if(response != null && response.Code == 1)
                            {
                                //3.上传成功更新结果
                                Global.ResultDb.Update<AlloyResultDao>()
                                    .Where(p=>p.ID == result.ID)
                                    .Set(p=>p.IsUpload,true)
                                    .Set(p=>p.UploadDateTime,DateTime.Now)
                                    .ExecuteAffrows();
                                Global.Logger.Info($"{result.ClientID} - {result.LotID} OK");
                            }
                            else
                            {
                                if(response.Msg != null)
                                {
                                    Global.Logger.Info($"{result.ClientID} - {result.LotID} Fail  Msg-{response.Msg}");
                                }
                                else
                                {
                                    Global.Logger.Info($"{result.ClientID} - {result.LotID} Fail  Code-{response.Code}");
                                }
                                
                                Thread.Sleep(3000);
                                continue;
                            }
                            
                        }
                        catch (Exception ex)
                        {
                            Thread.Sleep(3000);
                            Global.Logger.Error(ex,"上传数据异常");
                            //屏蔽上传异常
                            continue;
                        }  
                        
                    }
                }
                catch (Exception e)
                {
                    Global.Logger.Error(e,"上传数据时，查询数据失败");
                    return;
                }
            }
        }
        private static void ReceivedMessage(int command,object message)
        {
            switch (command)
            {
                case Command.RegisterClient:
                    var clientInfo = JsonConvert.DeserializeObject<ClientInfoDto>(message.ToString());

                    try
                    {
                        //注册终端
                        var request = new RestRequest("RegisterClient");
                        request.AddJsonBody(clientInfo);
                        Global.Logger.Info($"注册终端,IP:{clientInfo.IP} ID:{clientInfo.ClientId} MAC:{clientInfo.MAC}");
                        var response = restClient.Post<Message>(request);
                        if(response == null || response.Code != 1)
                        {
                           Global.Logger.Info($"注册终端失败，{response?.Msg}");
                           Global.SHSClien.RegisterClient_Reply(new ResultMessage(false,response?.Msg));
                        }
                        else
                        {
                            Global.Logger.Info($"注册终端成功");
                            Global.SHSClien.RegisterClient_Reply(new ResultMessage(true));
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.SHSClien.RegisterClient_Reply(new ResultMessage(false,ex.Message));
                    }
                    break;
                case Command.SendHeartbeat:
                    var heartbeat = JsonConvert.DeserializeObject<HeartbeatPackage>(message.ToString());
                    try
                    {
                        var request = new RestRequest("Heartbeat");
                        var response = restClient.Post<HeartbeatPackage>(request);
                        if(response != null)
                        {
                           Global.SHSClien.Heartbeat_Reply(response);
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    break;
            }
        }

        private static void TcpStateChanged(TcpState state)
        {
            Global.Logger.Info(state.State);
        }

    }
}
