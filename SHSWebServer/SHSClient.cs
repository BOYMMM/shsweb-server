﻿using SHSWebServer.DTO;
using SHSWebServer.Models;
using SocketTool;

namespace SHSWebServer
{
    public class SHSClient
    {
        private TcpClient tcpClient = null;

        public bool IsConnect   
        {
            get
            { 
                return tcpClient != null && tcpClient.IsConnect;
            } 
        }

        public SHSClient(TcpClient tcpClient)
        {
            this.tcpClient = tcpClient;
        }

        public void RegisterClient_Reply(ResultMessage message)
        {
            int command = Command.RegisterClient;
            this.tcpClient.ClearMessage(command);
            this.tcpClient.SendMessage(command,message);
        }

        public void Heartbeat_Reply(HeartbeatPackage message)
        {
            int command = Command.SendHeartbeat;
            this.tcpClient.ClearMessage(command);
            this.tcpClient.SendMessage(command,message);
        }

        public void SetLotInfo(LotInfoDto lotInfo,int timeOut = 1000)
        {
            int command = Command.SetLotInfo;
            this.tcpClient.ClearMessage(command);
            this.tcpClient.SendMessage(command,lotInfo);
            this.tcpClient.ReceiveMessage<ResultMessage>(command,timeOut,out var message);
            if (!message.IsSuccess)
            {
                throw new Exception(message.Info);
            }
        }
    }

    public class Command
    {
        public const int SetLotInfo = 1000;

        public const int RegisterClient = 2000;

        /// <summary>
        /// 向服务器发送心跳包
        /// </summary>
        public const int SendHeartbeat  = 3000;
    }
}
