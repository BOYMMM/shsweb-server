﻿using System.Collections.Concurrent;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using SHSWebServer.Models;
using SuperSimpleTcp;

namespace SocketTool
{
    public class TcpServer
    {
        private SimpleTcpServer server = null;
        private string localIP;
        private int port;
        private string clientIPPort;
        private ConcurrentDictionary<int, object> comDataDic;
        
        public event Action<TcpState> TcpStateChangedEvent;
        public event Action<int,object> ReceivedMessageEvent;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="localIp">127.0.0.1</param>
        /// <param name="port">10934</param>
        public TcpServer(string localIp, int port)
        {
            this.localIP = localIp;
            this.port = port;
            comDataDic = new ConcurrentDictionary<int, object>();
            server = new SimpleTcpServer($"{localIp}:{port}");
            server.Events.ClientConnected += ClientConnected;
            server.Events.ClientDisconnected += ClientDisconnected;
            server.Events.DataReceived += DataReceived;
        }

        public void Start()
        {
            server.Start();
        }
        
        public bool IsConnect => server != null && server.Connections > 0;

        private void DataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            string msg = Encoding.UTF8.GetString(e.Data.Array, 0, e.Data.Count);
            var tcpMessage = JsonConvert.DeserializeObject<TcpMessage>(msg);
            if (comDataDic.ContainsKey(tcpMessage.Command))
            {
                comDataDic.TryRemove(tcpMessage.Command, out var removeComData);
            }
            comDataDic.TryAdd(tcpMessage.Command, tcpMessage.JsonStr);
            ReceivedMessageEvent?.Invoke(tcpMessage.Command, tcpMessage.JsonStr);
        }

        private void ClientDisconnected(object sender, ConnectionEventArgs e)
        {
            TcpStateChangedEvent?.Invoke(new TcpState()
            {
                IsConnected = true,
                State = $"[{e.IpPort}] client disconnected: {e.Reason}"
            });
        }

        private void ClientConnected(object sender, ConnectionEventArgs e)
        {
            clientIPPort = e.IpPort;
            TcpStateChangedEvent?.Invoke(new TcpState()
            {
                IsConnected = true,
                State = $"[{e.IpPort}] client connected"
            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmdID"></param>
        /// <param name="timeOut"></param>
        /// <param name="jsonStr"></param>
        /// <returns></returns>
        public void ReceiveMessage<T>(int cmdID, double timeOut, out T recMsg)
        {
            recMsg = default;
            DateTime startTime = DateTime.Now;
            while (true)
            {
                if (DateTime.Now.Subtract(startTime).TotalMilliseconds > timeOut)
                    throw new TimeoutException();
                if (comDataDic.ContainsKey(cmdID))
                {
                    if (comDataDic.TryRemove(cmdID, out var recObject))
                    {
                        recMsg = JsonConvert.DeserializeObject<T>(recObject.ToString());
                        return;
                    }
                }
                Thread.Sleep(10);
            }
        }

        public void SendMessage(int command, object sendMsg)
        {
            var json =  JsonConvert.SerializeObject(sendMsg);
            TcpMessage tcpMessage = new TcpMessage(command,json);
            server.Send(this.clientIPPort,JsonConvert.SerializeObject(tcpMessage));
        }

        public void ClearMessage(int command)
        {
            if (this.comDataDic.ContainsKey(command))
            {
                this.comDataDic.TryRemove(command, out var data);
            }
        }
    }


    public class TcpClient
    {
        private SimpleTcpClient  client = null;
        private string localIP;
        private int port;
        private ConcurrentDictionary<int, object> comDataDic;
        
        public Action<TcpState> TcpStateChangedEvent;
        public Action<int,object> ReceivedMessageEvent;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="localIp">127.0.0.1</param>
        /// <param name="port">10934</param>
        public TcpClient(string localIp, int port)
        {
            this.localIP = localIp;
            this.port = port;
            comDataDic = new ConcurrentDictionary<int, object>();
            client = new SimpleTcpClient($"{localIp}:{port}");
            client.Events.Connected  += Connected;
            client.Events.Disconnected  += Disconnected;
            client.Events.DataReceived += DataReceived;
        }

        public void Connect()
        {
            client.Connect();
        }
        
        public bool IsConnect => client != null && client.IsConnected;
        private void DataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            string msg = Encoding.UTF8.GetString(e.Data.Array, 0, e.Data.Count);
            var tcpMessage = JsonConvert.DeserializeObject<TcpMessage>(msg);
            if (comDataDic.ContainsKey(tcpMessage.Command))
            {
                comDataDic.TryRemove(tcpMessage.Command, out var removeComData);
            }
            comDataDic.TryAdd(tcpMessage.Command, tcpMessage.JsonStr);
            ReceivedMessageEvent?.Invoke(tcpMessage.Command, tcpMessage.JsonStr);
        }

        private void Disconnected(object sender, ConnectionEventArgs e)
        {
            TcpStateChangedEvent?.Invoke(new TcpState()
            {
                IsConnected = true,
                State = $"Server {e.IpPort} disconnected"
            });
        }

        private void Connected(object sender, ConnectionEventArgs e)
        {
            TcpStateChangedEvent?.Invoke(new TcpState()
            {
                IsConnected = true,
                State = $"Server {e.IpPort} connected"
            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmdID"></param>
        /// <param name="timeOut"></param>
        /// <param name="jsonStr"></param>
        /// <returns></returns>
        public void ReceiveMessage<T>(int cmdID, double timeOut, out T recMsg)
        {
            recMsg = default;
            DateTime startTime = DateTime.Now;
            while (true)
            {
                if (DateTime.Now.Subtract(startTime).TotalMilliseconds > timeOut)
                    throw new TimeoutException();
                if (comDataDic.ContainsKey(cmdID))
                {
                    if (comDataDic.TryRemove(cmdID, out var recObject))
                    {
                        recMsg = JsonConvert.DeserializeObject<T>(recObject.ToString());
                        return;
                    }
                }
                Thread.Sleep(10);
            }
        }

        public void SendMessage(int command, object sendMsg)
        {
            var json =  JsonConvert.SerializeObject(sendMsg);
            TcpMessage tcpMessage = new TcpMessage(command,json);
            client.Send(JsonConvert.SerializeObject(tcpMessage));
        }

        public void ClearMessage(int command)
        {
            if (this.comDataDic.ContainsKey(command))
            {
                this.comDataDic.TryRemove(command, out var data);
            }
        }
    }

   
    public class TcpState
    {
        public bool IsConnected { get; set; }
        public string State { get; set; }
    }

    public class TcpMessage
    {
        public TcpMessage(int command,string jsonStr)
        {
            Command = command;
            JsonStr = jsonStr;
        }
        public int Command { get; set; }
        public string JsonStr { get; set; }
    }

    public class ResultMessage
    {
        public ResultMessage(bool isSuccess,string info = "")
        {
            this.IsSuccess = isSuccess;
            this.Info = info;
        }
        public bool IsSuccess  { get; set; }
        public string Info  { get; set; }
    }
}
